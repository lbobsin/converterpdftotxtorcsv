<?php 
namespace ExecuteActions;

use Files\Writer;
use Files\Output;

/**
 * Classe responsável por criar o arquivo
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class GenerateCreate
{
    /**    
     * Método construtor
     */  
    public function __construct()
    {
      
    }

    /**
	   * Método responsável por executar
	   * @access public
	   * 	 
     * @param Array $content
     * 
     * @param String $formatFile
     *
     * @param String $filename
     *    
     * @return Void
	 */ 
    public static function run(array $content ,string $formatFile )
    {
        $filenameError  = Output::createNameFile($content,"_error",$formatFile);
        $filename       = Output::createNameFile($content,"",$formatFile);
          
        Writer::deleteFile(Output::DIRECTORY.$filename);
        Writer::deleteFile(Output::DIRECTORY.$filenameError);      
    
        $file       = Writer::openFile(Output::DIRECTORY, $formatFile, $filename);
        $file_error = Writer::openFile(Output::DIRECTORY, $formatFile, $filenameError);        
          
        Output::writeHeaderCSV($content['registros'], $formatFile, $file);
        Output::writeHeaderCSV($content['registros'], $formatFile, $file_error);
    
        foreach ($content['registros'] as $registro){
    
           if($registro['tipo_simers'] == "NAO LOCALIZADO"){
             Output::write($formatFile, $registro, $file_error);                        
           }else{
             Output::write($formatFile, $registro, $file);
           }
        }
  
        Writer::closeFile($file_error);
        Writer::closeFile($file);      
    }      
}