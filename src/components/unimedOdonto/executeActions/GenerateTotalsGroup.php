<?php 
namespace ExecuteActions;

/**
 * Classe responsável por agrupar as informações por titular
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class GenerateTotalsGroup
{
    const RUBRICAS_COBRANCA  = array("Mensalidade","Premio");
    const RUBRICAS_DEVOLUCAO = array("Devolução");
    const INCREMENT          = 1;
    const TITULAR            = "T";

    /**    
     * Método construtor
     */  
    public function __construct()
    {
      
    }

    /**
     * Método responsável por executar
	 * @access public
	 * 	 
     * @param Array $content    
     *    
     * @return Array $titulares
	 */       
    public static function run(array $content) : array
    {
        $titulares = array();
        foreach ($content as $item) {

            if(trim($item['matricula']) != "" && trim($item['tipo']) == self::TITULAR){

                $titular['matricula']       = $item['matricula'];
                $titular['beneficiario']    = $item['beneficiario'];
                $titular['qtd_dependentes'] = 0;
                $titular['valor']           = (float) str_replace(",",".",trim($item['valor']));
                $titular['tipo_simers']     = $item['tipo_simers'];
                array_push($titulares, $titular);
            }
        }

        foreach ($titulares as $key => $titular) {

            foreach ($content as $item) {
                
                if((trim($item['tipo']) != self::TITULAR) && ($item['matricula'] == $titular['matricula'])){
                    $rubrica = explode(" ", trim($item['rubrica']));

                    if(isset($rubrica[0])){
                        $rubrica = $rubrica[0];
                    }
                    
                    if(in_array($rubrica, self::RUBRICAS_COBRANCA)){
                        $titulares[$key]['qtd_dependentes'] += self::INCREMENT;
                    }

                    if(in_array($rubrica, self::RUBRICAS_DEVOLUCAO)){
                        $titulares[$key]['valor']   -= (float) str_replace(",",".",trim($item['valor']));                        
                    }else{
                        $titulares[$key]['valor']   += (float) str_replace(",",".",trim($item['valor']));
                    }
                }
            }            
        }

        return $titulares;
    }    
}