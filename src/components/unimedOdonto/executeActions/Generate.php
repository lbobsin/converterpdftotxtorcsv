<?php 
namespace ExecuteActions;

use ExecuteActions\GenerateCreate;

/**
 * Classe responsável por gerar os arquivos finais com as informações
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class Generate
{
    /**    
     * Método construtor
     */  
    public function __construct()
    {
      
    }

    /**
     * Método responsável por executar
	   * @access public
	   * 	 
     * @param Array $content
     * 
     * @param String $formatFile
     *
     * @return Void
	   */       
    public static function run(array $content ,string $formatFile )
    { 
      GenerateCreate::run($content, $formatFile);
    }
}