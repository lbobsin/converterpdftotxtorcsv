<?php 
namespace ExecuteActions;

use Files\Writer;
use Files\Output;
use ExecuteActions\GenerateTotalsGroup;

/**
 * Classe responsável por criar o arquivo
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class GenerateTotalsCreate
{
    /**    
     * Método construtor
     */  
    public function __construct()
    {
      
    }

    /**
	 * Método responsável por executar
	 * @access public
	 * 	 
     * @param Array $content
     * 
     * @param String $formatFile
     *
     * @param String $filenameTotal
     *    
     * @return Void
	 */     
    public static function run(array $content, string $formatFile,string $filenameTotal)
    {
      $file_total   = Writer::openFile(Output::DIRECTORY, $formatFile, $filenameTotal);  
      $totals       = GenerateTotalsGroup::run($content);

      Output::writeHeaderCSV($totals, $formatFile, $file_total);
  
      foreach ($totals as $total) {        
        Output::write($formatFile, $total, $file_total);        
      }
      
      Writer::closeFile($file_total);    
    }    
}