<?php 
namespace ExecuteActions;

use Files\Writer;
use Files\Output;
use ExecuteActions\GenerateTotalsCreate;

/**
 * Classe responsável por gerar os arquivos finais com as informações totais por titular
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class GenerateTotals 
{
    /* 
	 * Método construtor 
	 */     
    public function __construct()
    {
      
    }

    /**
	 * Método responsável por executar
	 * @access public
	 * 	 
     * @param Array $content
     * 
     * @param String $formatFile
     *
     * @return Void
	 */      
    public static function run(array $content, string $formatFile)
    {
       $filenameTotal  = Output::createNameFile($content,"_total.",$formatFile);
       Writer::deleteFile(Output::DIRECTORY.$filenameTotal);
       GenerateTotalsCreate::run($content['registros'], $formatFile, $filenameTotal);
    }
}