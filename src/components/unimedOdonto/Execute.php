<?php 
namespace UnimedOdonto;

use Files\Directories;
use Files\Temporary;
use Manipulate\ExtractContent;
use Manipulate\NewContent;
use ExecuteActions\Generate;
use ExecuteActions\GenerateTotals;

/**
 * Classe responsável pela conversão de um arquivo traduzido do PDF
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class Execute 
{  
  protected $files = [];

  /* 
	 * Método construtor 
	 */  
  public function __construct()
  {
    
  }

  public static function process(string $formatFile) : array
  {
    $filesTemporaryProcess = Directories::getDirectoryAndNameFiles(Temporary::TYPES, Temporary::DIRECTORY);
    $contents 				     = NewContent::create(ExtractContent::extract($filesTemporaryProcess));   

    foreach ($contents as $content){
      Generate::run($content, $formatFile);
      GenerateTotals::run($content, $formatFile);
    }

    return $filesTemporaryProcess;
  }
}