<?php 
namespace Manipulate;

use Files\Writer;
use ExtractContentActions\AdjustBigWords;
use ExtractContentActions\IsNumericLastWord;
use ExtractContentActions\AdjustLayout;
use ExtractContentActions\AdjustTypeInSimersForDependency;

/**
 * Classe responsável por extrair as informações
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class ExtractContent
{    
    protected $files = [];

    /* 
	   * Método construtor 
	   */    
    public function __construct()
    {
      
    }
    
    /**
	   * Método responsável por fazer a extração das informações para tratamento
	   * @access public
	   * 	 
     * @param Array $files
     * 
	   * @return Array $mappedFiles
	   */    
    public static function extract(array $files = []) : array 
    {
      $mappedFiles = array();
      foreach ($files as $file) {
        $read 		  = $file['directory'].$file['filename'];
        $fileOpen 	= Writer::openFileForRead($read);
        $newFile 	  = array();        

        while (!feof($fileOpen)) {
           $item = Writer::readLine($fileOpen);
           $item = AdjustBigWords::run($item);
           $item = IsNumericLastWord::run($item);
  
           if($item != ""){
             $item = AdjustLayout::run($item);
             array_push($newFile, $item);          
           }
        }
  
        $newFile = AdjustTypeInSimersForDependency::run($newFile);       
  
        array_push($mappedFiles, array('filename' => $file['filename'], 'registros' => $newFile));
         
        Writer::closeFile($fileOpen);
        Writer::deleteFile($read);
      }
  
      return $mappedFiles;
    }
}