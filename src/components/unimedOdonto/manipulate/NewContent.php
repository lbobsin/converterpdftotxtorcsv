<?php
namespace Manipulate;

/**
 * Classe responsável por tratar as informações para o layout necessário
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class NewContent
{
    protected $files = [];

    /* 
	   * Método construtor 
	   */
    public function __construct()
    {
      
    } 

    /**
	   * Método responsável por fazer a chamada dos criadores dos arquivos finais
	   * @access public
	   * 	 
     * @param Array $contents
     * 
	   * @return Array $newContents
	   */
    public static function create(array $content = []) : array
    {
      $newContents = array();
  
      foreach ($content as $contents) {
        $new              = array();
        $newContentCreate = array();
  
        foreach ($contents['registros'] as $registro) {
          $newItem = self::adjust($registro); 
          array_push($newContentCreate, $newItem);
        }
  
        $new['filename']   = $contents['filename'];
        $new['registros']  = $newContentCreate;      
  
        array_push($newContents, $new);
      }
  
      return $newContents;
    }
  
    /**
	   * Método responsável por ajustar os campos criando um layout
	   * @access protected
	   * 	 
     * @param Array $item
     * 
	   * @return Array $newItem
	   */    
    protected static function adjust(array $item = []) : array
    {       
      $newItem['nro_beneficiario']= str_pad($item['nro_beneficiario'],  17, " ", STR_PAD_LEFT);
      $newItem['beneficiario']    = str_pad($item['beneficiario'],      40, " ", STR_PAD_LEFT);
      $newItem['matricula']       = str_pad($item['matricula'],         20, " ", STR_PAD_LEFT);
      $newItem['cpf']             = str_pad($item['cpf'],               14, " ", STR_PAD_LEFT);
      $newItem['plano']           = str_pad($item['plano'],             20, " ", STR_PAD_LEFT);
      $newItem['tipo']            = str_pad($item['tipo'],              20, " ", STR_PAD_LEFT);
      $newItem['idade']           = str_pad($item['idade'],              3, " ", STR_PAD_LEFT);
      $newItem['dependencia']     = str_pad($item['dependencia'],       15, " ", STR_PAD_LEFT);
      $newItem['data_inclusao']   = str_pad($item['data_inclusao'],     10, " ", STR_PAD_LEFT);
      $newItem['rubrica']         = str_pad($item['rubrica'],           40, " ", STR_PAD_LEFT);
      $newItem['valor']           = str_pad($item['valor'],             10, " ", STR_PAD_LEFT);
      $newItem['tipo_simers']     = str_pad($item['tipo_simers'],       14, " ", STR_PAD_LEFT);
      $newItem['finalizador']     = str_pad('1',                         2, " ", STR_PAD_LEFT);
      return $newItem;
    }  
}

