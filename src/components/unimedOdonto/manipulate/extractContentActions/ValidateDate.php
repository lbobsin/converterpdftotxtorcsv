<?php
namespace ExtractContentActions;

/**
 * Classe responsável por por verificar se a string é uma data
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class ValidateDate
{
    /* 
     * Método construtor 
	 */  
    public function __construct()
    {
      
    }  

    /**
	 * Método responsável por verificar se a string é uma data
	 * @access public
	 * 	 
     * @param String $string
     * 
	 * @return Array $data
	 */    
    public static function run(string $string) : array
    {
        return explode("/", $string);
    }    
}