<?php

namespace ExtractContentActions;

use ExtractContentActions\AdjustmentsRules;

/**
 * Classe responsável por identificar o tipo dos dependentes 
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class AdjustTypeInSimersForDependency
{
    protected $files = [];

    /* 
	   * Método construtor 
	   */  
    public function __construct()
    {
      
    }  
    
    /**
	   * Método responsável por executar
	   * @access public
	   * 	 
     * @param Array $itens
     * 
	   * @return Array $itens
	   */
    public static function run(array $itens = []) : array 
    {
      foreach ($itens as $chave => $item) {  
         if($item['tipo'] != AdjustmentsRules::TITULAR){
            $titular = self::localizeTitular($itens, $item['matricula']);
  
            if(!empty($titular)){  
              if($titular['tipo_simers'] == AdjustmentsRules::NAO_LOCALIZADO){
                $itens[$chave]['tipo_simers'] = AdjustmentsRules::NAO_LOCALIZADO;
              }else{
                $itens[$chave]['tipo_simers'] = "DEPENDENTE ".$titular['tipo_simers'];
              }  
            }
         }          
      }  
      
      return $itens;
    }
    
    /**
	   * Método responsável por localizar o titular no array pela matricula
	   * @access public
	   * 	 
     * @param Array $file    
     *
     * @param String $matricula
     * 
	   * @return Array $titular
	   */    
    protected static function localizeTitular(array $file = [], string $matricula) : array
    {
      $titular = array();

      if($matricula == ""){
        return $titular;
      }

      foreach ($file as $key => $value) {
        if($value['tipo'] == AdjustmentsRules::TITULAR && $value['matricula'] == $matricula){
          $titular = $value;
        }      
      }

      return $titular;
    }
}