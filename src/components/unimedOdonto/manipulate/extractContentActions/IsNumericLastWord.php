<?php

namespace ExtractContentActions;

use ExtractContentActions\AdjustmentsRules;

/**
 * Classe responsável por identificar a matricula unimed odonto do beneficario
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class IsNumericLastWord
{
    /* 
	   * Método construtor 
	   */   
    public function __construct()
    {
      
    }  
    
    /**
	   * Método responsável por executar
	   * @access public
	   * 	 
     * @param String $text
     * 
	   * @return String $text
	   */    
    public static function run(string $text) : string
    {
      $text     = str_replace("-", "", str_replace(".", "", $text));
      $lastWord = substr($text, -17).PHP_EOL;
  
      if(strlen($text) < 100){
        return "";
      }
  
      if(substr($lastWord, 0, 1) == AdjustmentsRules::CHAR_BEGIN_MATRICULA_UNIMED){
        return $text;
      }else{
        if(substr($lastWord, 2, 1) == AdjustmentsRules::CHAR_BEGIN_MATRICULA_UNIMED){
            return $text."10".PHP_EOL;
        }
      }   
  
      return "";
    }  
}