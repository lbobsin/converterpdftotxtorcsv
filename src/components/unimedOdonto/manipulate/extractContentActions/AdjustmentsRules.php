<?php

namespace ExtractContentActions;

/**
 * Classe responsável por manter todas as informações de valores a serem seguidos 
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class AdjustmentsRules
{
    const TITULAR                     = "T";
    const SIZE_CPF                    = 11;
    const PLANS                       = array( "ESSENCIAL PLUS ADESAO - RB", 
                                               "PLENO (SEM ORTO) ADESAO - RB", 
                                               "PLENO ORTO AD RB",
                                               "PLENO (SEM ORTO)", 
                                               "ESSENCIAL PLUS", 
                                               "PLENO ORTO AD RB");

    const MEDICO                      = "MEDICO";
    const ACADEMICO                   = "ACADEMICO";
    const FUNCIONARIO                 = "FUNCIONARIO";
    const NAO_LOCALIZADO              = "NAO LOCALIZADO";    
    
    const SIZE_DEFAULT_FIRST_EXPLODE  = 3;
    const SIZE_DEFAULT_MATRICULA      = 9;
    const CHAR_BEGIN_MATRICULA_UNIMED = "9";
    
    const TYPES_DEPENDENCY            = array("A","T","D");
    const INCREMENT_DEFAULT           = 1;
    const SIZESTRINGDATA              = 10;
    const REALOCATEBYINDEXONE         = 1;
    const REALOCATEBYINDEXTWO         = 2;

    /* 
     * Método construtor      
	 */  
    public function __construct()
    {
        
    }      
}
