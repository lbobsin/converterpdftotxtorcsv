<?php
namespace ExtractContentActions;

use ExtractContentActions\ValidateDate;
use ExtractContentActions\AdjustmentsRules;

/**
 * Classe responsável por ajustar o array, caso o indice 1 ou 2 seja uma data
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class RelocatesValuesFromArray
{
    const INDEX_DEFAULT = 2;
    /* 
     * Método construtor 
	 */  
    public function __construct()
    {
      
    }  

    /**
	 * Método responsável por executar
	 * @access public
	 * 	 
     * @param Array $array
     * 
     * @param Integer $realocate
     * 
	 * @return Array $data
	 */     
    public static function run(Array $array, int $realocate) : array
    {
        if(is_array(ValidateDate::run($array[$realocate]))){

            $temporaryArray = $array;

            if(strlen($array[$realocate]) == AdjustmentsRules::SIZESTRINGDATA){
                if($realocate == AdjustmentsRules::REALOCATEBYINDEXONE){
                  $array[$realocate]     = " ";
                  $array[$realocate + 1] = " ";
                  $indice                = self::INDEX_DEFAULT;
                }
    
                if($realocate == AdjustmentsRules::REALOCATEBYINDEXTWO){
                  $array[$realocate]     = " ";
                  $indice                = self::INDEX_DEFAULT;
                }            
    
                $array[$indice + 1] = $temporaryArray[$realocate];
                $array[$indice + 2] = $temporaryArray[$realocate + 1];
                $array[$indice + 3] = $temporaryArray[$realocate + 2];
                $array[$indice + 4] = $temporaryArray[$realocate + 3];
                $array[$indice + 5] = (isset($temporaryArray[$realocate + 4])?$temporaryArray[$realocate + 4]:null);
            }
        }

        return $array;
    }    
}