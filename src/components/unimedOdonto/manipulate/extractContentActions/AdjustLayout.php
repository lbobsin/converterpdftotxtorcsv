<?php

namespace ExtractContentActions;

use Model\Saas;
use ExtractContentActions\AdjustFirst;
use ExtractContentActions\AdjustSecond;
use ExtractContentActions\AdjustmentsRules;

/**
 * Classe responsável por pegar o texto que esta maior que 200 e pegar as informações até o tamanho 124
 * que é o necessário. 
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class AdjustLayout
{
    /* 
	   * Método construtor 
	   */  
    public function __construct()
    {
      
    }  
    
    /**
	   * Método responsável por executar
	   * @access public
	   * 	 
     * @param String $text
     * 
	   * @return Array $item
	   */    
    public static function run(string $text) : array
    {  
      $item = array();
  
      foreach (AdjustmentsRules::PLANS as $plan) {
        $planIdentify = strpos($text, $plan);        
  
        if($planIdentify != false){
          $explode                    = explode($plan, $text);          
          $explodeFirst               = AdjustFirst::run($explode);
          $explodeSecond              = AdjustSecond::run($explode);
          
          $item['nro_beneficiario']   = (isset($explodeSecond[7])?$explodeSecond[7]:$explodeSecond[6]);
          $item['beneficiario']       = $explodeFirst[0];
          $item['matricula']          = $explodeFirst[1];
          $item['cpf']                = $explodeFirst[2];
          $item['plano']              = $plan;
          $item['tipo']               = $explodeSecond[0];
          $item['idade']              = $explodeSecond[1];
          $item['dependencia']        = str_replace("Ã", "A",$explodeSecond[2]);
          $item['data_inclusao']      = $explodeSecond[3];
          $item['rubrica']            = $explodeSecond[4];
          $item['valor']              = $explodeSecond[5];
          $item['tipo_simers']        = self::getTypeInSimers($item);
        }
      }
      return $item;      
    }

    /**
	   * Método responsável por fazer a consulta das entidades no sistema
	   * @access protected
	   * 	 
     * @param Array $item
     * 
	   * @return String $entidade_tipo_id
	   */    
    protected static function getTypeInSimers(array $item = []) : string
    {
      if( $item['tipo'] == AdjustmentsRules::TITULAR ){
  
        if( (strlen($item['cpf']) < AdjustmentsRules::SIZE_CPF) && ($item['cpf'] != "") ){
          $item['cpf'] = str_pad($item['cpf'],  AdjustmentsRules::SIZE_CPF, "0", STR_PAD_LEFT);
        }
        
        if( $item['cpf'] != "" ){
          $entidade = Saas::selectForCpf($item['cpf']);          
  
          switch ($entidade['entidade_tipo_id']) {
            case 1:  return AdjustmentsRules::MEDICO; break;
            case 3:  return AdjustmentsRules::ACADEMICO; break;
            case 5:  return AdjustmentsRules::FUNCIONARIO; break;      
            default: return AdjustmentsRules::NAO_LOCALIZADO; break;
          }        
        }
      }
  
      return AdjustmentsRules::NAO_LOCALIZADO;
    }    
}