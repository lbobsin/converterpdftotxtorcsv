<?php
namespace ExtractContentActions;

use ExtractContentActions\AdjustmentsRules;

/**
 * Classe responsável por por dividir a informação em um primeiro lote
 * que é o necessário. 
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class AdjustFirst
{
    /* 
     * Método construtor 
	   */  
    public function __construct()
    {
      
    }

    /**
     * Método responsável por executar
	   * @access public
	   * 	 
     * @param Array $explode
     * 
	   * @return Array $explodeFirst
	   */   
    public static function run(array $explode) : array
    {
        $explodeFirst = preg_split("/[\t]/", trim($explode[0]));
  
        if(sizeof($explodeFirst) < AdjustmentsRules::SIZE_DEFAULT_FIRST_EXPLODE){
          $temporaryExplodeFirst = $explodeFirst;
    
          if(strlen($explodeFirst[1]) < AdjustmentsRules::SIZE_DEFAULT_MATRICULA){            
            $explodeFirst[2] = "";
          }else{            
            $explodeFirst[1] = "";
            $explodeFirst[2] = $temporaryExplodeFirst[1];
          }
        }
    
        return $explodeFirst;
    }       
}