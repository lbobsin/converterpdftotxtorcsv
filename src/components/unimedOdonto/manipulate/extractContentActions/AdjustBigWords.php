<?php
namespace ExtractContentActions;

/**
 * Classe responsável por pegar o texto que esta maior que 200 e pegar as informações até o tamanho 124
 * que é o necessário. 
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class AdjustBigWords
{
    /* 
     * Método construtor 
	   */  
    public function __construct()
    {
      
    }  
    
    /**
	   * Método responsável por executar
	   * @access public
	   * 	 
     * @param String $text
     * 
	   * @return String $text
	   */      
    public static function run(string $text) : string
    {
      if(strlen($text) > 200){
        $text = trim(substr($text, 124, strlen($text)));                  
      }
  
      return $text;
    } 
}