<?php
namespace ExtractContentActions;

use ExtractContentActions\RelocatesValuesFromArray;
use ExtractContentActions\AdjustmentsRules;

/**
 * Classe responsável por por dividir a informação em um segundo lote
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class AdjustSecond
{
    /* 
     * Método construtor      
	   */  
    public function __construct()
    {
      
    }  

    /**
     * Método responsável por executar
	   * @access public
	   * 	 
     * @param Array $explode
     * 
	   * @return Array $explodeSecond
	   */   
    public static function run(array $explode) : array
    {
        $explodeSecond = preg_split("/[\t]/", trim($explode[1]));
        
        if(!in_array($explodeSecond[0], AdjustmentsRules::TYPES_DEPENDENCY)){
          foreach ($explodeSecond as $key => $itemExplodeSecond) {            

            if(isset($explodeSecond[$key + AdjustmentsRules::INCREMENT_DEFAULT])){
               $explodeSecond[$key] = $explodeSecond[$key + AdjustmentsRules::INCREMENT_DEFAULT];   
            }

            if(empty($explodeSecond[$key])){
               unset($explodeSecond[$key]);
            }
          }
        }

        $explodeSecond = RelocatesValuesFromArray::run($explodeSecond, AdjustmentsRules::REALOCATEBYINDEXTWO);
        $explodeSecond = RelocatesValuesFromArray::run($explodeSecond, AdjustmentsRules::REALOCATEBYINDEXONE);

        return $explodeSecond;  
    }    
}