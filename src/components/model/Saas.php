<?php 
namespace Model;

/**
 * Classe responsável pela conexão com o banco de dados
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class Saas
{
    /**
     * Método construtor
     */
    public function __construct()
    {
	  self::setConnection();
    }

	/**
	 * Método de inicialização do objeto de conexão com o banco de dados
	 * @access protected
	 * @return object
	 */
    protected static function setConnection()
    {   	
   	  return new \Slim\PDO\Database(getenv("ENV_DNS"), getenv("ENV_USERNAME"), getenv("ENV_PASSWORD"));
    }

	/**
	 * Método responsável por retornar as informações de uma entidade pelo cpf da mesma
	 * @access public
	 * 
	 * @param String $cpf
	 * @return Array $entidade
	 */
    public static function selectForCpf(string $cpf)
    {
   		$db = self::setConnection();   		

		$selectStatement = $db->select(array("*"))
      		  				->from("entidade")
      		  				->join("entidade_fisica", "entidade.entidade_id", "=", "entidade_fisica.entidade_id")
      		  				->where("replace(replace(entidade_fisica.entidade_fisica_cpf,'.',''),'-','')","=",$cpf)
      		  				->orderBy('entidade.entidade_tipo_id', 'ASC');

      	$stmt = $selectStatement->execute();
		return $stmt->fetch();
    }
}
