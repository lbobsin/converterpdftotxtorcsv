<?php 
namespace Files;

/**
 * Classe responsável pela manutenção dos arquivos da pasta de temporários
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class Temporary 
{  
  const TYPES             = array('txt','TXT','csv', 'CSV');  
  const DIRECTORY        =  "/mnt/file/temporary/";  

   /**    
    * Método construtor
    */
   public function __construct()
   {
    
   } 
}