<?php 
namespace Files;

use Files\Writer;

/**
 * Classe responsável pela manutenção dos arquivos da pasta de saida
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class Output 
{  
  const	TYPES 			 = array('pdf','PDF');
  const DIRECTORY        =  "/mnt/file/output/";  

   /**
    * Método construtor
    */
   public function __construct()
   {
    
   }

   /**
	 * Método responsável por retornar os campos para montagem do cabeçalho do arquivo CSV
     * @access protected
     *     
     * 
     * @param Array $array
	 * 	 
	 * @return Array $files
    */
   protected function getHeaderCSV(array $array) : array
   {
       $fields = array();
       foreach ($array as $values) {           
           foreach ($values as $key => $value) {
               array_push($fields, $key);
           }
           return $fields;
       }
   }

   /**
	 * Método responsável por adicionar o header no arquivo CSV
     * @access public     
     * 
     * @param Array $array
     * 
     * @param String $formaFile
     * 
     * @param Resource $file
	 * 	 
	 * @return Boolean
    */   
   public static function writeHeaderCSV(array $array, string $formatFile , $file) : bool
   {        
        $separator = ";";
        if(strtoupper($formatFile) != "CSV"){
            return false;
        }

        $fields = self::getHeaderCSV($array);

        $text = "";
        foreach ($fields as $field) {
            $text .= $field.$separator;
        }
        $text .= PHP_EOL;
        echo $text;
        Writer::writeFile($file, $text);
        return true;
   }   

   /**
	 * Método responsável por escrever no arquivo
     * @access public     
     * 
     * @param String $formatFile
     * 
     * @param Array $registro
     * 
     * @param Resource $file
	 * 	 
	 * @return Boolean
    */      
   public static function write(string $formatFile ,array $registro = [] , $file) : bool
   {
        $separator = "";
        if(strtoupper($formatFile) == "CSV"){
            $separator = ";";
        }        
        $text = implode($separator, $registro).$separator.PHP_EOL;
        Writer::writeFile($file, $text);

        return true;
   }

    /**
	 * Método responsável por gerar o nome do arquivo a ser utilizado
     * @access public     
     * 
     * @param String $formatFile
     * 
     * @param Array $file
     * 
     * @param String $complement
     * 
     * @param String $formatFile
	 * 	 
	 * @return String $filename
    */      
   public static function createNameFile(array $file ,string $complement = '',string $formatFile ) : string
   {       
        $filename       = explode(".", $file['filename']);
        return $filename[0].$complement.".".$formatFile;
   }
}