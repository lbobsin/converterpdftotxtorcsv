<?php 

namespace Files;

use Files\Directories;

/**
 * Classe responsável pela manutenção dos arquivos da pasta de entrada
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class Input 
{  
  const	TYPES 			 = array('pdf','PDF');
  const DIRECTORY        =  "/mnt/file/input/";  

   /**
     * Método construtor
     */    
   public function __construct()
   {
    
   }

   /**
	 * Método responsável por retornar as informações dos arquivos na pasta de entrada
	 * @access public
	 * 	 
	 * @return Array $files
	 */
   public static function getFiles() : array
   {       
   	return Directories::getDirectoryAndNameFiles(self::TYPES, self::DIRECTORY);
   }   
}