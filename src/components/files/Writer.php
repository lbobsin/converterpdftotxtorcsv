<?php 
namespace Files;

use Smalot\PdfParser\Parser;
use Files\Temporary;

/**
 * Classe responsável pela manutenção dos arquivos de conversão
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class Writer 
{
  protected $files = [];

  /**
   * Método construtor
   */  
  public function __construct()
  {
   
  }

  /**
	 * Método responsável por criar e escrever nos arquivos de conversão
	 * @access public
	 * 	
   * @param String $formatFile
   * 
   * @param Array $files
   *  
	 * @return Boolean
	 */  
  public static function process(string $formatFile, array $files = [])
  {
     $filesWithTexts = self::getTextFromFiles($files);

     foreach ($filesWithTexts as $file) {       
       $writtenFile = self::openFile(Temporary::DIRECTORY, $formatFile, $file['filename']);
       
       if(is_resource($writtenFile)){

        foreach ($file['text'] as $text) {          
          self::writeFile($writtenFile, $text->getText());
        }        
        self::closeFile($writtenFile);
       }
     }

     return true;
  }   

  /**
	 * Método responsável por pegar as informações de um array de informações obtidas de um PDF 
	 * @access protected	   
   * 
   * @param Array $files
   *  
	 * @return Array $mappedTextFiles
	 */ 
  protected static function getTextFromFiles(array $files = []) : array 
  {
     $mappedTextFiles = array();
     foreach ($files as $file) {
       $read = $file['directory'].$file['filename'];

       $parser        = new Parser(); 
       $pdf           = $parser->parseFile($read);
       $file['text']  = $pdf->getPages();
       array_push($mappedTextFiles, $file);
     }     
     return $mappedTextFiles;
  }
  
  /**
	 * Método responsável por criar um arquivo para iniciar a escrita
	 * @access public	   
   * 
   * @param String $directory
   * 
   * @param String $formatFile
   * 
   * @param String $filename
   *  
	 * @return Resource
	 */   
  public static function openFile(string $directory, string $formatFile, string $filename)
  {    
    $filename     = explode(".", $filename);
    return fopen ($directory.$filename[0].".".$formatFile,"a+");
  }

  /**
	 * Método responsável por abrir um determinado arquivo para a escrita
	 * @access public	   
   * 
   * @param String $file
   *  
	 * @return Resource
	 */  
  public static function openFileForRead(string $file)
  { 
    return fopen ($file,"r");
  }  

  /**
	 * Método responsável por escrever um determinado arquivo
	 * @access public	   
   * 
   * @param Resource $file
   * 
   * @param String $text
   *  
	 * @return Resource
	 */  
  public static function writeFile($file, string $text)
  {    
    return fwrite($file , $text);
  }

  /**
	 * Método responsável por fechar um determinado arquivo
	 * @access public	   
   * 
   * @param Resource $file     
   *  
	 * @return Resource
	 */
  public static function closeFile($file)
  {
    return fclose($file);
  } 

  /**
	 * Método responsável por ler um determinado arquivo
	 * @access public	   
   * 
   * @param Resource $file     
   *  
	 * @return String $line
	 */
  public static function readLine($file)
  { 
    $line = fgets($file, 4096);
    return trim($line);
  }

  /**
	 * Método responsável por deletar um determinado arquivo
	 * @access public	   
   * 
   * @param Resource $file     
   *  
	 * @return Void
	 */
  public static function deleteFile(string $file)
  {     
    if(file_exists($file) == true){
       echo "Exclusão do arquivo anterior:".$file."--> Ok!".PHP_EOL;
        unlink($file);    
    }
  }    
}

