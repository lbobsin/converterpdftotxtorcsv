<?php 
namespace Files;

/**
 * Classe responsável pela manutenção dos direitorios
 * @author Luciano Bobsin <lucianosouzabobsin@gmail.com>
 */
class Directories 
{  

  /**
   * Método construtor
   */
  public function __construct(){
   
  }

  /**
	 * Método responsável por retornar as informações dos arquivos no diretório especificado
	 * @access public
	 * 	
   * @param Array $type
   * 
   * @param String $directory
   *  
	 * @return Array $files
	 */
  public  static function getDirectoryAndNameFiles(array $type = [], string $directory) : array
  {
    $dir = new \DirectoryIterator($directory);   

    $mappedFiles = array();
    foreach ($dir as $fileInfo) {
        $extension = strtolower( $fileInfo->getExtension() );
        if( in_array( $extension, $type ) ){
          $mapped = ['filename' => $fileInfo->getFilename(),'directory' => $directory];
          array_push($mappedFiles, $mapped);
        }
    }
    return $mappedFiles;
  }  
}

